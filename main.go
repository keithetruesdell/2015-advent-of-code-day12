package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

const (
	ex1   string = "input/ex1.txt"
	ex2   string = "input/ex2.txt"
	ex3   string = "input/ex3.txt"
	ex4   string = "input/ex4.txt"
	ex5   string = "input/ex5.txt"
	ex6   string = "input/ex6.txt"
	ex7   string = "input/ex7.txt"
	ex8   string = "input/ex8.txt"
	input string = "input/input.txt"
)

func main() {
	var (
		answer string
		err    error
	)

	ex := getFlags()
	f2r := file2use(ex)
	data, err := readfile(f2r)
	if err != nil {
		fmt.Printf("ERROR reading the file: \n    %v\n", err)
	}

	jsonStruct := fmtData(data)

	fmt.Println("==========================================")
	fmt.Printf("  Answer: %v\n", answer)
}

// fmtData -
func fmtData(data []string) fmtD {

	return fmtD
}

// readfile -
func readfile(f2r string) (data []string, err error) {
	inpF, err := os.Open(f2r)
	if err != nil {
		return data, err
	}

	defer inpF.Close()

	scn := *bufio.NewScanner(inpF)

	for scn.Scan() {
		ln := strings.TrimSpace(scn.Text())
		data = append(data, ln)
	}

	return data, err
}

// file2use -
func file2use(ex int) (f2r string) {
	switch ex {
	case 0:
		return input
	case 1:
		return ex1
	case 2:
		return ex2
	case 3:
		return ex3
	case 4:
		return ex4
	case 5:
		return ex5
	case 6:
		return ex6
	case 7:
		return ex7
	case 8:
		return ex8
	default:
		return ex1
	}
}

// getFlags -
func getFlags() (ex int) {
	tmpEx := flag.Int("e", 1, "What example to use? 1 is the default, 0 is production.")
	flag.Parse()
	ex = *tmpEx
	return ex
}
